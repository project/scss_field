# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2023-02-17
### Changed
- updated info file to correct the module info for a correct install

## [2.0.0] - 2023-01-11
### Changed
- Change branch version number to align with semantic versioning.

## [0.0.2] - 2023-01-11

### Fixed

- e9774ce Issue #3199137 by lathan: Multiple SCSS fields via paragraphs don't all include SCSS
- 8403e14 Issue #3201012 by lathan: Invalid supplied SCSS, white screen untapped exception
- 41226f0 kicking off D9 version of SCSS Field module

## [0.0.1] - 2023-01-10

### Added

- This CHANGELOG file
- Added myself as a maintainer
- Cut a few branch for the 9.x

### Fixed

- Replace leafo/scssphp with scssphp/scssphp (da176d18)
- Automated Drupal Rector fixes (bbe7472e)


